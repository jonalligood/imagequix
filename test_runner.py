from unittest import TextTestRunner, TextTestResult
from unittest.signals import registerResult
import warnings


class CSVTestResult(TextTestResult):
    """
    A custom unittest TestResult that writes errors, failures, and successes
    in a CSV file format for CSVTestRunner.
    """
    def __init__(self, stream, descriptions, verbosity):
        super(CSVTestResult, self).__init__(stream, descriptions, verbosity)
        self.successes = []

    def addError(self, test, err):
        """
        We skip over TextTestResult to directly inherit from TestResult. This
        way we avoid printing 'E' to the CSV when encountering an error.
        """
        super(TextTestResult, self).addError(test, err)

    def addFailure(self, test, err):
        """
        We skip over TextTestResult to directly inherit from TestResult. This
        way we avoid printing 'F' to the CSV when encountering an error.
        """
        super(TextTestResult, self).addFailure(test, err)

    def addSuccess(self, test):
        self.successes.append((test, True))

    def print_results(self):
        self.print_result_list(self.successes, was_successful=True)
        self.print_result_list(self.errors, was_successful=False)
        self.print_result_list(self.failures, was_successful=False)

    def print_result_list(self, result_list, was_successful):
        for test, res in result_list:
            desc = test._testMethodDoc.strip()
            # Replace double quotes with single quotes to properly escape
            # the traceback.
            if res is True:
                cleaned_result = ''
            else:
                cleaned_result = res.replace('"', '\'')
            self.stream.writeln('{}, {}, "{}"'.format(desc, was_successful,
                                                      cleaned_result))


class CSVTestRunner(TextTestRunner):
    """
    A custom test runner based on the unittest.TextTestRunner.
    Writes the results to the specified CSV file.

    Note: All tests are required to have an appropriate docstring
    """
    resultclass = CSVTestResult

    def run(self, test):
        "Run the given test case or suite."
        result = self._makeResult()
        registerResult(result)
        result.failfast = self.failfast
        result.buffer = self.buffer
        result.tb_locals = self.tb_locals
        with warnings.catch_warnings():
            if self.warnings:
                # If self.warnings is set, use it to filter all the warnings
                warnings.simplefilter(self.warnings)
                # If the filter is 'default' or 'always', special-case the
                # warnings from the deprecated unittest methods to show them
                # no more than once per module, because they can be fairly
                # noisy. The -Wd and -Wa flags can be used to bypass this
                # only when self.warnings is None.
                if self.warnings in ['default', 'always']:
                    warnings.filterwarnings(
                        'module',
                        category=DeprecationWarning,
                        message=r'Please use assert\w+ instead.'
                    )

            startTestRun = getattr(result, 'startTestRun', None)
            if startTestRun is not None:
                startTestRun()
            try:
                test(result)
            finally:
                stopTestRun = getattr(result, 'stopTestRun', None)
                if stopTestRun is not None:
                    stopTestRun()

        self.stream.write("Test Case, Success, Traceback\n")
        result.print_results()
        return result

