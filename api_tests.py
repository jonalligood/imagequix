import argparse
from datetime import datetime, timedelta
import unittest
from unittest import TestCase

import requests

from test_runner import CSVTestRunner

DOMAIN = 'https://{}.herokuapp.com'
TEST_SUBDOMAIN = 'imagequix-qa-interview'
STAGE_SUBDOMAIN = 'not-a-real-site'

PAYLOAD_KEYS = ['id', 'title', 'body', 'publishDate', 'lastUpdated']


def get_blog_id(url):
    """
    Given a url, returns the id of the first element
    """
    blog_list = requests.get(url).json()
    if blog_list:
        return blog_list[0]['id']


class ApiTestCase(TestCase):
    url = None

    @classmethod
    def parse_url(cls):
        """
        Handles url configuration for the test suite. Ifthe user passes
        --staging while calling the script, it will use the staging web
        service, otherwise it defaults to the test web service.
        """
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '--staging',
            help='Use the staging server for API tests',
            action='store_true'
        )

        args = parser.parse_args()

        if args.staging:
            BASE_URL = DOMAIN.format(STAGE_SUBDOMAIN)
        else:
            BASE_URL = DOMAIN.format(TEST_SUBDOMAIN)

        print("Running tests against {}".format(BASE_URL))

        cls.url = BASE_URL


class GetTests(ApiTestCase):
    def setUp(self):
        self.get_url = self.url + '/blog-posts'

    def test_get_all_blogs(self):
        """
        GET all blog posts
        """
        r = requests.get(self.get_url)

        self.assertEqual(r.status_code, 200)

        # Ensure an array is returned
        self.assertEqual(type(r.json()), list)

        # TODO: Ensure blogs match expected payload

    def test_get_individual_blog(self):
        """
        GET a specific blog
        """
        blog_id = get_blog_id(self.get_url)

        if blog_id:
            blog_url = self.get_url + '/{}'.format(blog_id)
            r = requests.get(blog_url)

            self.assertEqual(r.status_code, 200)

            for key in PAYLOAD_KEYS:
                self.assertIn(key, r.json())

            # TODO: parse publishDate and lastUpdated for expected format


class PostTests(ApiTestCase):
    def setUp(self):
        self.post_url = self.get_url = self.url + '/blog-posts'
        delta = timedelta(days=5)
        future_date = datetime.now() + delta
        self.payload = {
            'title': 'API Test Title',
            'body': '<p>Test Body</p>',
            'publishDate': future_date.isoformat()[:-3]  # Convert to millisec
        }

    def tearDown(self):
        """
        Delete the created blog if it exists
        """
        get_blogs = requests.get(self.get_url)
        for blog in get_blogs.json():
            if self.payload['title'] == blog['title']:
                blog_id = blog['id']
                requests.delete(self.url + '/blog-posts/{}'.format(blog_id))

    def test_post_a_blog(self):
        """
        POST a blog to /blog-posts
        """
        posted_blog = requests.post(self.post_url, data=self.payload)

        self.assertEqual(posted_blog.status_code, 201)

        # Check to see if the data has successfully been posted
        get_blogs = requests.get(self.get_url)
        for blog in get_blogs.json():
            if self.payload['title'] == blog['title']:
                break
        else:
            # We looped through all the blogs and the data isn't there
            self.fail('The posted data was not found in /blog-posts')


class PutTests(ApiTestCase):
    def setUp(self):
        get_url = self.url + '/blog-posts'
        blog_id = get_blog_id(get_url)
        self.put_url = get_url + '/{}'.format(blog_id)

        delta = timedelta(days=5)
        future_date = datetime.now() + delta
        self.payload = {
            'title': 'API Test Title',
            'body': '<p>Test Body</p>',
            'publishDate': future_date.isoformat()[:-3]  # Convert to millisec
        }

    def test_put_a_blog(self):
        """
        Update a blog using PUT
        """
        updated_blog = requests.put(self.put_url, data=self.payload)

        self.assertEqual(updated_blog.status_code, 200)

        # Confirm data has been updated
        get_blog = requests.get(self.put_url)
        self.assertEqual(get_blog['title'], self.payload['title'])
        self.assertEqual(get_blog['body'], self.payload['body'])
        self.assertEqual(get_blog['publishDate'], self.payload['publishDate'])


class DeleteTests(ApiTestCase):
    def setUp(self):
        # TODO: Ideally, the deleted blog would be one we add during setUp
        self.get_url = self.url + '/blog-posts'
        self.blog_id = get_blog_id(self.get_url)
        self.delete_url = self.get_url + '/{}'.format(self.blog_id)

    def test_delete_a_blog(self):
        """
        Delete the specified blog
        """
        deleted = requests.delete(self.delete_url)

        self.assertEqual(deleted.status_code, 204)

        # Confirm the blog is no longer in the list
        blog_list = requests.get(self.get_url)
        for blog in blog_list.json():
            if blog['id'] == self.blog_id:
                self.fail('Deleted blog id found in list of blogs.')


if __name__ == '__main__':
    ApiTestCase.parse_url()
    with open('results.csv', 'w') as f:
        unittest.main(testRunner=CSVTestRunner(f))
