# Installation

```
pip install -r requirements.txt
```

Note: As of right now, this code base is only Python 3 compatible.

# Running Tests

```
python api_tests.py
```

This will produce results.csv where the results of the test are located.

Looking at results.csv, we see that GET and DELETE methods are successful. POST and PUT, meanwhile, runs into errors related to the payload.
